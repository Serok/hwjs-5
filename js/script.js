// ВОПРОС:
// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования.
// ОТВЕТ:
// Для отображения некоторых (спец и др.) символов в строках, для вызова функций, переменных в строках.
// Другими словами, экранирование нужно, чтобы элемент "вырвать из потока" или переопределить тип элемента в строке.

// ЗАДАНИЕ:
// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
// Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//     - Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и
//       дополните ее следующим функционалом:
//     - При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и
//       сохранить ее в поле birthday.
//     - Создать метод getAge() который будет возвращать сколько пользователю лет.
//     - Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре,
//       соединенную с фамилией (в нижнем регистре) и годом рождения.
//       (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//     - Вывести в консоль результат работы функции createNewUser(), а также
//       функций getAge() и getPassword() созданного объекта.

function createNewUser() {
    this.firstName = prompt('Enter you first name: ', '');
    while (this.firstName === '' || isNaN(this.firstName) !== true) {
        this.firstName = prompt('Enter you first name AGAIN: ', '');
    }

    this.lastName = prompt('Enter you last name', '');
    while (this.lastName === '' || isNaN(this.lastName) !== true) {
        this.lastName = prompt('Enter you last name AGAIN: ', '');
    }

    let birthday = prompt('Enter your age (dd.mm.yyyy)', '');
    let dateArray = birthday.split('.');

    this.getLogin = function () {
        let newLogin = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        return newLogin;
    }

    this.getAge = function () {
        let birthDate = new Date(`${dateArray[2]}-${dateArray[1]}-${dateArray[0]}`);
        let today = new Date();
        let age = today.getFullYear() - birthDate.getFullYear();
        let m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    this.getPassword = function () {
        let newPassword = this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + dateArray[2];
        return newPassword;
    }
}

let newUser = new createNewUser();
console.log(`login: ${newUser.getLogin()}`);
console.log(`password: ${newUser.getPassword()}`);
console.log(`age: ${newUser.getAge()}`);
